package com.shukur.exchange_bot.client;

import com.shukur.exchange_bot.exception.ServiceException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Component
public class CbrClient {
    /*@Autowired
    private OkHttpClient client;

    private final String beforeUrl = "https://www.cbar.az/currencies/";

    LocalDate today = LocalDate.now();

    //LocalDate yesterday = today.minusDays(1);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    String formattedDate = today.format(formatter);

    private final String url = beforeUrl + formattedDate + ".xml";

    public Optional<String> getCurencyRatesXml() throws ServiceException {
        var request = new Request.Builder()
                .url(url)
                .build();

        try (var response = client.newCall(request).execute();) {
            var body =response.body();
            return body == null ? Optional.empty() : Optional.of(body.string());
        } catch (IOException e) {
            throw new ServiceException("Something wrong with currency API", e);
        }
    }*/

    @Autowired
    private OkHttpClient client;

    private final String beforeUrl = "https://www.cbar.az/currencies/";
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public Optional<String> getCurencyRatesXml() throws ServiceException {
        LocalDate date = LocalDate.now();

        Optional<String> responseBody = checkForData(date);
        while (responseBody.isEmpty()) {
            date = date.minusDays(1);
            responseBody = checkForData(date);

            if (date.isBefore(LocalDate.now().minusDays(7))) {
                throw new ServiceException("Data not available for the past 7 days.");
            }
        }

        return responseBody;
    }

    private Optional<String> checkForData(LocalDate date) {
        String formattedDate = date.format(formatter);
        String url = beforeUrl + formattedDate + ".xml";

        var request = new Request.Builder()
                .url(url)
                .build();

        try (var response = client.newCall(request).execute()) {
            var body = response.body();
            return body == null ? Optional.empty() : Optional.of(body.string());
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}

