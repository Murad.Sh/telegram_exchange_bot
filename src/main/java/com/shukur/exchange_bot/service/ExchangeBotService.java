package com.shukur.exchange_bot.service;

import com.shukur.exchange_bot.exception.ServiceException;

public interface ExchangeBotService {
    String getUsdExchange() throws ServiceException;

    String getEurExchange() throws ServiceException;

    void clearUSDCache();

    void clearEURCache();
}
