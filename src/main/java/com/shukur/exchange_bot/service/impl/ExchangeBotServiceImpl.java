package com.shukur.exchange_bot.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.shukur.exchange_bot.client.CbrClient;
import com.shukur.exchange_bot.exception.ServiceException;
import com.shukur.exchange_bot.service.ExchangeBotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;

@Service
public class ExchangeBotServiceImpl implements ExchangeBotService {

    private static final Logger LOG = LoggerFactory.getLogger(ExchangeBotServiceImpl.class);
    private static final String USD_XPATH = "/ValCurs//ValType[@Type='Xarici valyutalar']//Valute[@Code='USD']/Value";
    private static final String EUR_XPATH = "/ValCurs//ValType[@Type='Xarici valyutalar']//Valute[@Code='EUR']/Value";

    @Autowired
    private CbrClient client;

    @Cacheable(value = "usd", unless = "#result == null or #result.isEmpty()")

    @Override
    public String getUsdExchange() throws ServiceException {
        var xmlOptional = client.getCurencyRatesXml();
        String xml = xmlOptional.orElseThrow(
                () -> new ServiceException("Не удалось получить XML")
        );
        return extractCurrencyValueFromXml(xml, USD_XPATH);
    }

    @Cacheable(value = "eur", unless = "#result == null or #result.isEmpty()")
    @Override
    public String getEurExchange() throws ServiceException {
        var xmlOptional = client.getCurencyRatesXml();
        String xml = xmlOptional.orElseThrow(
                () -> new ServiceException("Не удалось получить XML")
        );
        return extractCurrencyValueFromXml(xml, EUR_XPATH);
    }

    @CacheEvict("usd")
    @Override
    public void clearUSDCache() {
        LOG.info("Cache \"usd\" cleared!");
    }

    @CacheEvict("eur")
    @Override
    public void clearEURCache() {
        LOG.info("Cache \"eur\" cleared!");
    }

    private static String extractCurrencyValueFromXml (String xml, String xpathExpression) throws ServiceException {
        var source = new InputSource(new StringReader(xml));

        try {
            var xpath = XPathFactory.newInstance().newXPath();
            var document = (Document) xpath.evaluate("/", source, XPathConstants.NODE);

            return xpath.evaluate(xpathExpression, document);
        } catch (XPathExpressionException e) {
            throw new ServiceException("Can't open XML file", e);
        }
    }
}
